from struct_svm_solver_bundle import *
from struct_svm_solver_bundle_scipy import *
from struct_svm_solver_dual import *
from struct_svm_solver_1_slack import *
import numpy as np
import gurobipy
import argparse
import time
import utils
# import dlib


class funkey_problem(object):
    C = 1
    be_verbose = True
    learns_nonnegative_weights = True
    epsilon = 0.001

    def __init__(self,
                 features_filename,
                 constraints_filename,
                 labels_filename,
                 loss_weights=None):
        self._gurobi_model = gurobipy.Model("funkey_problem")
        self._gurobi_variables = []

        self._ground_truth_label = []
        self._ground_truth_label_class = []
        self._loss_weights = loss_weights
        if loss_weights == None:
            self._loss_weights = [1]
        self._feature_matrix = np.zeros(0)

        # load data into the model
        self._load_features(features_filename)
        self._load_constraints(constraints_filename)
        self._load_ground_truth(labels_filename)

        # set parameters read from the solver:
        self.num_samples = 1
        self.num_dimensions = self._feature_matrix.shape[1]

        # which solver is used?
        self.use_dlib = False

    def make_psi(self, label):
        label = np.array(label)
        if self.use_dlib:
            return dlib.vector(np.dot(label, self._feature_matrix))
        else:
            return np.dot(label, self._feature_matrix)

    def get_truth_joint_feature_vector(self, idx):
        assert(idx == 0)
        return self.make_psi(self._ground_truth_label)

    def get_value_and_gradient(self, current_weights):
        """
        Callback for bundle method
        :param current_weights:
        :return:
        """
        solution, value = self.separation_oracle(0, current_weights, True)
        return value, self._get_gradient(solution)

    def _get_gradient(self, label):
        return np.dot(self._ground_truth_label, self._feature_matrix) - np.dot(label, self._feature_matrix)

    def separation_oracle(self, idx, current_weights, label_only=False):
        """
        Then the separation oracle finds a Y such that:
            Y = argmax over all y: LOSS(idx,y) + F(X,y) - F(X,idx)
                                                        ^^^^^^^^^^^
                                                       only if using own solver

            (i.e. It finds the label which maximizes the above expression.)

        Finally, separation_oracle() returns LOSS(idx,Y),PSI(X,Y)
        """
        assert(idx == 0)
        print("\n========================================\n\nSeparation Oracle called with weights: {}".format(current_weights))
        start_time = time.time()

        # build objective function by multiplying features and weights
        coefficients = np.dot(self._feature_matrix, np.array(current_weights))

        # we minimize the energy so we have to subtract the loss
        # loss is split as in funkey's sbmrm into coefficients and constant offset
        loss_coefficients = np.array([-1 if l == 1 else 1 for l in self._ground_truth_label]) * self._ground_truth_label_class
        loss_augmented_coefficients = loss_coefficients - coefficients
        print("\tcoefficients computed")

        # set objective
        objective = gurobipy.LinExpr(loss_augmented_coefficients, self._gurobi_variables)
        objective += np.dot(self._ground_truth_label_class, self._ground_truth_label)  # constant term of loss

        if not self.use_dlib:
            objective += np.dot(coefficients, np.array(self._ground_truth_label))

        # minimize
        print("\tLinear Expression constructed, setting objective")
        self._gurobi_model.setObjective(objective, gurobipy.GRB.MAXIMIZE)
        print("\tStarting optimization")
        self._gurobi_model.optimize()

        # extract solution
        solution = [int(v.getAttr(gurobipy.GRB.attr.X)) for v in self._gurobi_variables]

        # timing
        end_time = time.time()
        print("\tSeparation oracle took {} secs\n\n========================================\n".format(end_time - start_time))

        if not label_only:
            return utils.weighted_hamming_loss(solution,
                                           self._ground_truth_label,
                                           self._ground_truth_label_class), self.make_psi(solution)
        else:
            return solution, self._gurobi_model.objVal

    def _load_features(self, features_filename):
        """
        Loads psi(x)', one column per line, from a file in the following form:

        features.txt:

          # one feature vectors per line

          0.2 1 # for y_0
          0.1 2 # for y_1
          0.3 0 # ...
          0.1 2
        """
        with open(features_filename, 'rt') as features_file:
            num_weights = -1
            feature_matrix = []

            for line in features_file:
                line = utils.remove_comments_from_line(line)

                # ignore if line is a comment or empty
                if len(line) == 0 or line[0] == '#':
                    continue

                line_psi = [float(x) for x in line.split()]

                if num_weights < 0:
                    num_weights = len(line_psi)
                else:
                    assert(len(line_psi) == num_weights)

                feature_matrix.append(line_psi)

            self._feature_matrix = np.array(feature_matrix)

            print("Loaded feature matrix of size {}".format(self._feature_matrix.shape))

    def _load_constraints(self, constraints_filename):
        """
        Loads linear constraints on y from a file in the following form:

        constraints.txt:

          # format: [ceofficient*component]* [rel] [value]
          # components of y starting counting at 0
          # [rel] = <= | == | >=

          1*0 1*1 1*2 1*3 >= 1 # at least one compnent of y has to be 1
        """
        with open(constraints_filename, 'rt') as constraint_file:
            variables = set()
            constraints = []

            for line in constraint_file:
                line = utils.remove_comments_from_line(line)

                # ignore if line is a comment or empty
                if len(line) == 0 or line[0] == '#':
                    continue

                # split line
                pos = line.rindex('=')
                relation = line[pos - 1:pos + 1]
                lhs = line[:pos - 1].strip()
                rhs = line[pos + 1:].strip()

                # find all participating variables and their coefficients
                coeff_var_list = []
                for coeff_var_pair in lhs.split():
                    coeff, var = coeff_var_pair.split('*')
                    coeff_var_list.append((float(coeff), int(var)))
                    variables.add(int(var))

                constraints.append((coeff_var_list, relation, float(rhs)))

            # add all variables to gurobi
            self._gurobi_variables = [self._gurobi_model.addVar(vtype=gurobipy.GRB.BINARY) for _ in variables]
            self._gurobi_model.update()

            # add all constraints to gurobi
            for coeff_var_list, rel, rhs in constraints:
                lhs = sum([coeff * self._gurobi_variables[var] for coeff, var in coeff_var_list])
                if rel == "==":
                    self._gurobi_model.addConstr(lhs == rhs)
                elif rel == "<=":
                    self._gurobi_model.addConstr(lhs <= rhs)
                elif rel == ">=":
                    self._gurobi_model.addConstr(lhs >= rhs)
            self._gurobi_model.update()

            print("Set up problem with {} variables and {} constraints".format(len(variables), len(constraints)))

    def _load_ground_truth(self, labels_filename):
        """
        Loads the ground truth labeling y' from a file of the format:

        labels.txt:

          0 #c0\t # one digit per component of y', optionally followed by a class #c
          1 # class 0 is assumed if none given
          0
          1
        """
        with open(labels_filename, 'rt') as gt_file:
            self._ground_truth_label = []

            label_classes = set()
            label_classes.add(0)

            for line in gt_file:
                # find class configuration starting with '#c' followed by int
                label_class = 0
                if '#c' in line:
                    label_class = int(line[line.index('#c') + 2:line.index('\t', line.index('#c'))])
                    label_classes.add(label_class)

                line = utils.remove_comments_from_line(line)

                # ignore if line is a comment or empty
                if len(line) == 0 or line[0] == '#':
                    continue

                # add to ground truth vector
                self._ground_truth_label.append(int(line))
                self._ground_truth_label_class.append(label_class)

            # make sure that we have a weight given for all the classes that are present
            for c in label_classes:
                assert(c < len(self._loss_weights))

            self._ground_truth_label_class = [self._loss_weights[c] for c in self._ground_truth_label_class]

            print("Loaded a ground truth labeling for {} variables".format(len(self._ground_truth_label)))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Funkey Learning interface to Carstens and the DLIB SSVM solver")
    parser.add_argument('--features', dest='features_filename', type=str, default="features.txt", help='Filename for features')
    parser.add_argument('--constraints', dest='constraints_filename', type=str, default="constraints.txt", help='Filename for constraints')
    parser.add_argument('--labels', dest='labels_filename', type=str, default="labels.txt", help='Filename for labels')
    parser.add_argument('--loss_weights', metavar='loss_weights', type=float, nargs='+', help="Weight loss of labels depending on their class, all the same if not specified.")
    parser.add_argument('--output', dest='weights_filename', type=str, default='weights.txt', help='Weights output filename')
    parser.add_argument('--dlib', dest='use_dlib', default=False, action='store_true', help='Set this option to solve using dlib instead of own solver')
    args = parser.parse_args()

    # problem = funkey_problem(args.features_filename, args.constraints_filename, args.labels_filename, [1,1,1,1,1]))
    problem = funkey_problem(args.features_filename, args.constraints_filename, args.labels_filename, args.loss_weights)
    if args.use_dlib:
        import dlib
        problem.use_dlib = True
        weights = dlib.solve_structural_svm_problem(problem)
    else:
        # solver = struct_svm_solver_dual(problem)
        # solver = struct_svm_solver_1_slack(problem)
        solver = struct_svm_solver_bundle(problem)
        # solver = struct_svm_solver_bundle_scipy(problem)
        weights = solver.solve()

    print("Found weights: {}".format(weights))
    outfile = file(args.weights_filename, 'wt')
    outfile.write("{}\n".format(weights))
    outfile.close()

