#!/usr/bin/env python
import argparse
import numpy as np
from struct_svm_solver_primal import *
import utils

class struct_svm_ranking_problem():
    C = 1
    be_verbose = True
    learns_nonnegative_weights = True

    def __init__(self,
                 features_filename,
                 fixed_loss_filename,
                 gt_labels_filename,
                 proposal_filenames,
                 loss_weights=None):

        self.num_samples = 1
        self.num_proposals = 0
        self.use_feature_normalization = True
        self._means = np.zeros(1)
        self._variances = np.zeros(1)

        if fixed_loss_filename != '':
            self._losses = np.loadtxt(fixed_loss_filename)
            self.num_proposals = len(self._losses)
            print("Found {} losses in file {}.".format(self._losses.shape, fixed_loss_filename))
        else:
            self._ground_truth_label = []
            self._ground_truth_label_class_multiplier_list = []
            self._loss_weights = loss_weights
            if loss_weights == None:
                self._loss_weights = [1]

            self._load_ground_truth(gt_labels_filename)
            self._proposals = np.zeros(1)
            self._load_proposals(proposal_filenames)

            self._losses = np.array([utils.multiclass_weighted_hamming_loss(p,
                                                       self._ground_truth_label,
                                                       self._ground_truth_label_class_multiplier_list,
                                                       self._loss_weights) for p in self._proposals])

        self._proposal_features = np.zeros(1)
        self._load_features(features_filename)
        self.num_dimensions = self._proposal_features.shape[1]

    def get_best_proposal_for_sample(self, sample_idx):
        assert(sample_idx == 0)

        return self._losses.argmin()

    def get_proposal_feature_vector(self, sample_idx, proposal_idx):
        assert(sample_idx == 0)
        return self._proposal_features[proposal_idx]

    def get_proposal_loss(self, sample_idx, proposal_idx):
        assert(sample_idx == 0)
        return self._losses[proposal_idx]

    def get_num_proposals_for_sample(self, sample_idx):
        assert(sample_idx == 0)
        return self.num_proposals

    def get_feature_normalization_values(self):
        """
        If we normalized the features before training the SVM, this method returns mean and variance per feature.
        :return: two numpy arrays, one for means and one for variances
        """
        assert(self.use_feature_normalization)
        return self._means, self._variances

    def _load_features(self, features_filename):
        """
        Loads the features computed for each proposalfrom a file of the format:

        proposal_features.txt:

          0.1 3.2 # feature 0 for each proposal
          1.6 7.9 # feature 1 for each proposal
          0.5 1.2 # ....
          1.0 1.0
        """
        with open(features_filename, 'rt') as features_file:
            proposal_features = []

            for line in features_file:
                line = utils.remove_comments_from_line(line)

                # ignore if line is a comment or empty
                if len(line) == 0 or line[0] == '#':
                    continue

                # read feature for all proposals and check that we always have the same number
                proposals = line.split()

                assert(self.num_proposals == len(proposals))

                proposal_features.append(map(float, proposals))

            self._proposal_features = np.array(proposal_features).transpose()
            print("Loaded feature vectors of length {} with min={} and max={}".format(self._proposal_features.shape[1],
                                                                                      np.min(self._proposal_features),
                                                                                      np.max(self._proposal_features)))
        if self.use_feature_normalization:
            print("Normalizing features")
            self._means, self._variances = utils.normalize_feature_matrix(self._proposal_features)
            print("Normalized feature matrix has min={} and max={}".format(np.min(self._proposal_features),
                                                                           np.max(self._proposal_features)))

    def _load_proposals(self, proposal_filenames):
        """
        Loads the proposal labelings, each from a file in the same format as the ground truth,
        but classes are ignored.

        proposals.txt:

          0 # label for one indicator variable per line
          1 # comments are allowed
          1
          1
        """
        proposals = []
        for proposal_filename in proposal_filenames:
            print("\tReading {} ...".format(proposal_filename))
            proposal_labeling = utils.load_proposal_labeling(proposal_filename)
            assert(len(proposal_labeling) == len(self._ground_truth_label))
            proposals.append(proposal_labeling)

        self.num_proposals = len(proposal_filenames)
        self._proposals = np.array(proposals)
        print("Loaded {} proposal labelings for {} variables".format(self.num_proposals, self._proposals.shape[1]))

    def _load_ground_truth(self, labels_filename):
        self._ground_truth_label, self._ground_truth_label_class_multiplier_list, label_classes = \
            utils.load_labelfile_with_classes_and_multipliers(labels_filename)

        print("Ground truth has length {}".format(len(self._ground_truth_label)))

        # make sure that we have a weight given for all the classes that are present
        for c in label_classes:
            assert(c < len(self._loss_weights))

        print("Loaded a ground truth labeling for {} variables".format(len(self._ground_truth_label)))

    def print_scores(self, weights):
        for i in range(self.num_proposals):
            score = np.dot(weights, self._proposal_features[i])
            loss = self._losses[i]
            print("Proposal {} has score {} and loss {}".format(i, score, loss))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Struct-SVM ranking of proposals. Solves the problem:\n"
                                                 "min |w|^2 + C * sum(slacks)\n"
                                                 "where C is the regularizer-weight and w are the learned weights.")
    parser.add_argument('--features', dest='features_filename', type=str, default="features.txt",
                        help='Filename for features per proposal')
    parser.add_argument('--labels', dest='labels_filename', type=str, default="labels.txt",
                        help='Filename for ground truth label')
    parser.add_argument('--proposals', dest='proposal_filenames', type=str, nargs='+', default=["proposal_0.txt","proposal_1.txt"],
                        help='Filenames for proposals')
    parser.add_argument('--loss_weights', dest='loss_weights', type=float, nargs='+',
                        help="Weight loss of labels depending on their class, all the same if not specified.")
    parser.add_argument('--fixed-losses', dest='fixed_losses', type=str, default='',
                        help="File containing the same number of loss values as there are proposals. Makes ground truth obsolete!")
    parser.add_argument('--output', dest='weights_filename', type=str, default='weights.txt',
                        help='Weights output filename (means and variances are appended)')
    parser.add_argument('--regularizer-weight', dest='regularizer_weight', type=float, default=1.0,
                        help='Weight of regularizer')

    args = parser.parse_args()

    problem = struct_svm_ranking_problem(args.features_filename,
                                         args.fixed_losses,
                                         args.labels_filename,
                                         args.proposal_filenames,
                                         args.loss_weights)
    problem.C = args.regularizer_weight
    problem.use_feature_normalization = True
    solver = struct_svm_solver_primal(problem)
    weights = solver.solve()

    weights = np.array(weights)

    problem.print_scores(weights)

    print("Found weights: {}".format(weights))
    if len(args.weights_filename) > 0:
        print("Saving weights to: " + args.weights_filename)
        np.savetxt(args.weights_filename, weights)
        import os.path as path
        m, v = problem.get_feature_normalization_values()
        np.savetxt(path.splitext(args.weights_filename)[0] + '_means.txt', m)
        np.savetxt(path.splitext(args.weights_filename)[0] + '_variances.txt', v)