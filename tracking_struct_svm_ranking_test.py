#!/usr/bin/python

import numpy as np
from struct_svm_solver_primal import *
import utils

def main():
    unaries = np.array([[0.2, 0.7, 0.1],
                        [0.1, 0.85, 0.05], 
                        [0.3, 0.6, 0.1],
                        [0.4, 0.5, 0.1],
                        [0.05, 0.05, 0.9],
                        [0.3, 0.4, 0.3],
                        [0.15, 0.7, 0.15]])

    edges = [(0, 2), (1, 3), (0, 3), (1, 2), (2, 4), (3, 4), (4, 5), (4, 6)]

    samples = [unaries]
    labels_ground_truth = [np.array([1, 1, 1, 1, 2, 1, 1])]
    labels_proposals = [[np.array([2, 0, 2, 0, 2, 1, 1]),
                         np.array([0, 2, 2, 0, 1, 0, 2]),
                         np.array([1, 1, 1, 1, 1, 1, 1]),
                        np.array([1, 1, 1, 1, 2, 1, 1])]]  # include gt?

    problem = tracking_problem_ranking(samples, labels_ground_truth, labels_proposals, edges)

    print("\nBegin training\n")
    # this is where all the stuff below gets called

    solver = struct_svm_solver_primal(problem)
    weights = solver.solve()

    print("\nDone training!\n")
    print("StructSVM training produced weights: {}".format(np.array(weights)))
    for s in range(len(samples)):
        for i in range(len(labels_proposals[s])):
            print "score for proposal[{}]: {}".format(i,
                                            np.dot(weights, problem.make_psi(samples[0], labels_proposals[s][i])))


class tracking_problem_ranking:
    # define SVM regularization strength
    C = 1
    be_verbose = True
    # learns_nonnegative_weights = True

    def __init__(self, samples, labels_ground_truth, labels_proposals, edges):
        self.num_samples = len(samples)
        self.num_dimensions = 2

        self.__samples = samples
        self.__labels_ground_truth = labels_ground_truth
        self.__labels_proposals = labels_proposals
        self.__edges = edges

    def make_psi(self, x, label):
        """Compute PSI(x,label)."""
        psi = np.zeros(self.num_dimensions)

        psi[0] = sum([x[i][int(y)] for i, y in enumerate(label)])  # sum of unaries incurred with this labeling
        psi[1] = sum([abs(label[a] - label[b]) for a, b in self.__edges])  # pairwise terms

        print("Psi for labeling {} is: {}".format(np.array(label), np.array(psi)))

        return psi

    def get_best_proposal_for_sample(self, sample_idx):
        # TODO: build something automatic here? how do we choose the best of the proposals?
        # is it the one with lowest loss? lowest MAP energy?
        assert(sample_idx == 0)
        return 3

    def get_truth_joint_feature_vector(self, idx):
        return self.make_psi(self.__samples[idx], self.__labels_ground_truth[idx])

    def get_proposal_feature_vector(self, sample_idx, proposal_idx):
        return self.make_psi(self.__samples[sample_idx], self.__labels_proposals[sample_idx][proposal_idx])

    def get_proposal_loss(self, sample_idx, proposal_idx):
        return utils.loss(self.__labels_ground_truth[sample_idx], self.__labels_proposals[sample_idx][proposal_idx])

    def get_num_proposals_for_sample(self, sample_idx):
        return len(self.__labels_proposals[sample_idx])


if __name__ == "__main__":
    main()

