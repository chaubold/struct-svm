#!/usr/bin/python
#
# COMPILING THE DLIB PYTHON INTERFACE
#   Dlib comes with a compiled python interface for python 2.7 on MS Windows.  If
#   you are using another python version or operating system then you need to
#   compile the dlib python interface before you can use this file.  To do this,
#   run compile_dlib_python_module.bat.  This should work on any operating system
#   so long as you have CMake and boost-python installed.  On Ubuntu, this can be
#   done easily by running the command:  sudo apt-get install libboost-python-dev cmake


import dlib
import opengm
import numpy as np
from struct_svm_solver_dual import *


def build_model(unaries, edges, weights=[100.0, 1.0], loss=None):
    """
    Build an OpenGM model that represents our tracking graph with unaries and pairwise potentials
    :param unaries:
    :param edges:
    :param weights:
    :param loss:
    """
    num_variables = unaries.shape[0]
    num_states = unaries.shape[1]

    if loss != None:
        assert(loss.shape[0] == unaries.shape[0] and loss.shape[1] == unaries.shape[1])
    
    variables_space = np.ones(num_variables) * num_states
    gm = opengm.gm(variables_space)

    # compute unaries
    unary_tables = np.zeros((num_variables, num_states), dtype='double')
    for i in range(num_variables):
        
        unaryFunction = -np.log(unaries[i, :]) * weights[0]

        if loss != None:
            # the loss is subtracted because instead of max loss(y_opt,y) + w*psi(x,y)
            # we perform energy minimization min E(x,y|w)  - loss(y_opt, y)
            unaryFunction -= loss[i, :]
        unary_tables[i, :] = unaryFunction

    # shift unaries such that 0 is the minimum
    # unary_tables -= unary_tables.min()
    print(unary_tables)

    # add unaries to model
    for i in range(num_variables):
        # add unary function to graphical model
        functionId = gm.addFunction(unary_tables[i, :])

        # add unary factor to graphical model
        gm.addFactor(functionId, i)

    if weights[1] > 0.0:
        print("Adding edges to model")
        # Add Potts-regularizer - not potts any more!
        beta = weights[1]
        regularizerFunction = np.zeros([3, 3])
        regularizerFunction[0, 1] = beta
        regularizerFunction[0, 2] = 2 * beta
        regularizerFunction[1, 0] = beta
        regularizerFunction[1, 2] = beta
        regularizerFunction[2, 0] = 2 * beta
        regularizerFunction[2, 1] = beta

        # add 2. order function to graphical model
        # but only ONCE
        regularizerFunctionId = gm.addFunction(regularizerFunction)

        # add factors for the edges
        for a, b in edges:
            gm.addFactor(regularizerFunctionId, [a, b])

    return gm


def get_labels(gm):
    """
    Perform inference on the model and return the labels
    """
    inference_type = "cplex"
    optimizer = None

    if "icm" == inference_type:
        optimizer = opengm.inference.Icm(gm=gm)
    elif "lf" == inference_type:
        optimizer = opengm.inference.LazyFlipper(gm=gm)
    elif "cplex" == inference_type:
        # cplex with integer constraint
        param = opengm.InfParam(integerConstraint=True, numberOfThreads=1)
        optimizer = opengm.inference.LpCplex(gm=gm, parameter=param)
    elif "fusionBp" == inference_type:
        # Thorsten suggested that we try this on our tracking model.
        # He successfully ran it on Bernhard's chaingraph from the OpenGM benchmark
        bpParam = opengm.InfParam(
            steps=100,
            damping=0.2
        )
        infParam = opengm.InfParam(
            generator='bp',
            fusionSolver='lf',
            maxSubgraphSize=3,
            infParam=bpParam
        )
        optimizer = opengm.inference.SelfFusion(gm, parameter=infParam)


    visitor = optimizer.verboseVisitor(printNth=1, multiline=True)
    
    # run inference
    optimizer.setStartingPoint(np.ones(7, dtype='uint') * 2)  # start with all labels = 2
    optimizer.infer(releaseGil=True)
    #optimizer.infer(visitor, releaseGil=True)

    print("Running inference {} yielded {} with energy {}".format(inference_type, optimizer.arg(), optimizer.value()))
    return optimizer.arg()

def main():
    unaries = np.array([[0.2, 0.7, 0.1],
                        [0.1, 0.85, 0.05], 
                        [0.3, 0.6, 0.1],
                        [0.4, 0.5, 0.1],
                        [0.05, 0.05, 0.9],
                        [0.3, 0.4, 0.3],
                        [0.15, 0.7, 0.15]])

    unaries2 = np.array([[0.2, 0.7, 0.1],
                        [0.1, 0.85, 0.05],
                        [0.3, 0.6, 0.1],
                        [0.4, 0.5, 0.1],
                        [0.1, 0.1, 0.8],
                        [0.3, 0.4, 0.3],
                        [0.15, 0.7, 0.15]])

    edges = [(0, 2), (1, 3), (0, 3), (1, 2), (2, 4), (3, 4), (4, 5), (4, 6)]

    samples = [dlib.matrix(unaries), dlib.matrix(unaries2)]
    labels = [dlib.vector(get_labels(build_model(unaries, edges)).astype('double')),
              dlib.vector(get_labels(build_model(unaries2, edges)).astype('double'))]
    #labels = [dlib.vector(np.array([1, 1, 1, 1, 2, 1, 1]).astype('double'))]

    problem = tracking_problem(samples, labels, edges)

    print("\nBegin training\n")
    # this is where all the stuff below gets called
    # weights = dlib.solve_structural_svm_problem(problem)
    solver = struct_svm_solver_dual(problem)
    weights = solver.solve([0.0, 0.0])

    print("\nDone training!\n")
    print("StructSVM training produced weights: {}".format(np.array(weights)))
    for i in range(len(samples)):
        print "predicted label for sample[{}]: {}".format(i, np.array(predict_label(weights, edges, samples[i])))


def predict_label(weights, edges, sample):
    return dlib.vector(get_labels(build_model(np.array(sample), edges, weights)).astype('double'))

###########################################################################################


class tracking_problem:
    # define SVM regularization strength
    C = 1
    be_verbose = True
    # learns_nonnegative_weights = True

    def __init__(self, samples, labels, edges):
        self.num_samples = len(samples)
        self.num_dimensions = 2

        self.__samples = samples
        self.__labels = labels
        self.__edges = edges

    def make_psi(self, x, label):
        """Compute PSI(x,label)."""
        psi = dlib.vector()
        psi.resize(self.num_dimensions)

        psi[0] = sum([x[i][int(y)] for i, y in enumerate(label)])  # sum of unaries incurred with this labeling
        psi[1] = sum([abs(label[a] - label[b]) for a, b in self.__edges])

        print("Psi for labeling {} is: {}".format(np.array(label), np.array(psi)))

        return psi

    def get_truth_joint_feature_vector(self, idx):
        return self.make_psi(self.__samples[idx], self.__labels[idx])

    @staticmethod
    def hamming_loss(labels_a, labels_b):
        # Compute the hamming loss for two given solutions
        """
        compute hamming loss for two labelings
        :param labels_a:
        :param labels_b:
        """
        # print("Computing Hamming Loss for labels {} and {}".format(np.array(labels_a), np.array(labels_b)))
        return sum([abs(a-b) for a,b in zip(labels_a, labels_b)])

    def separation_oracle(self, idx, current_solution):
        #               Then the separation oracle finds a Y such that:
        #                   Y = argmax over all y: LOSS(idx,y) + F(X,y)
        #                   (i.e. It finds the label which maximizes the above expression.)
        #
        #               Finally, separation_oracle() returns LOSS(idx,Y),PSI(X,Y)
        print("Separation Oracle called with weights: {}".format(np.array(current_solution)))

        sample = self.__samples[idx]
        label = self.__labels[idx]

        # construct loss which will be added to the unaries of our loss_augmented_model
        loss = np.ones(sample.shape)
        for i, j in enumerate(label):
            for l in range(3):
                loss[i, l] = abs(j - l)

        #print("Incurring loss: {}".format(loss))

        # we usually do energy minimization, so we could understand our energy as -F(x,y|w) in max_y F(x,y|w).
        loss_augmented_model = build_model(np.array(sample), self.__edges, current_solution, loss)
        max_scoring_label = dlib.vector(get_labels(loss_augmented_model).astype('double'))

        # Finally, return the loss and PSI vector corresponding to the label we just found.
        psi = self.make_psi(sample, max_scoring_label)
        hloss = utils.hamming_loss(max_scoring_label, label)
        return hloss, psi

if __name__ == "__main__":
    main()

