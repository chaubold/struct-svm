import gurobipy
import numpy as np
import time

class struct_svm_solver_1_slack:
    """
    A solver to a struct SVM problem, formulated in the same way as for dlib.
    To reproduce the call "weights = dlib.solve_structural_svm_problem(problem)",
    create an instance of this class and call solve(), which returns the weights.

    It is not intended to be fast, but to serve as example implementation.

    @author: Carsten Haubold
    """

    def __init__(self, problem):
        """
        """

        # parameters of the SSVM problem
        self.__problem = problem
        try:
            self.__epsilon = problem.epsilon
        except:
            print("Epsilon not set in given struct svm problem, using 0.001")

        # nonnegative weights?
        # TODO: use this flag!
        self.__learn_nonnegative_weights = True
        try:
            self.__learn_nonnegative_weights = problem.learns_nonnegative_weights
        except:
            print("Problem does not specify whether non-negative weights should be learned. Setting to True.")

        # Gurobi model for the n-slack svm
        self.__model = gurobipy.Model("struct_svm")
        self.__weights = [self.__model.addVar() for i in range(problem.num_dimensions)]
        self.__slack = self.__model.addVar()
        self.__epsilon = 0.001
        self.__constraints_per_sample = {}
        self.__psi_gt = [problem.get_truth_joint_feature_vector(i) for i in range(problem.num_samples)]

        # setup QP:
        self.__model.update()
        obj = gurobipy.QuadExpr()

        for w_i in self.__weights:
            obj += w_i * w_i

        self.__model.setObjective(0.5 * obj + self.__problem.C * self.__slack, gurobipy.GRB.MINIMIZE)

    def solve(self):
        """
        Solve the quadratic problem of a 1-slack SVM with margin rescaling using gurobi,
        instanciating constraints in a cutting plane approach, as explained in
        "Cutting Plane Training of Structural SVMs"
        by Joachims, Algorithm 4

        Currently it only computes nonnegative weights!
        """
        num_samples = self.__problem.num_samples
        slack_exceeded = True
        num_iterations = 0
        weights = []

        start_time = time.time()

        while slack_exceeded:
            num_iterations += 1

            # solve current QP and extract weights and slack
            self.__model.optimize()
            weights = [wi.getAttr(gurobipy.GRB.attr.X) for wi in self.__weights]
            xsi = self.__slack.getAttr(gurobipy.GRB.attr.X)

            # get most violated constraint per sample
            psis = []
            losses = []
            for i in range(num_samples):
                # find most violated constraint (H(y) in paper)
                loss, psi = self.__problem.separation_oracle(i, weights)
                psi = np.array(psi)
                psis.append(psi)
                losses.append(loss)

            # add constraint to gurobi
            delta_psi_sum = np.sum([losses[i] * (self.__psi_gt[i] - psis[i]) for i in range(num_samples)], axis=0)
            self.__model.addConstr(np.dot(self.__weights, delta_psi_sum)
                                   >= sum(losses) - float(num_samples) * self.__slack)

            self.__model.update()

            # check exit condition:
            if sum(losses) + np.dot(weights, delta_psi_sum) > float(num_samples) * (xsi + self.__epsilon):
                slack_exceeded = True
            else:
                slack_exceeded = False

        print("Number of iterations: {}".format(num_iterations))
        end_time = time.time()
        print("Done in {} secs".format(end_time - start_time))

        return weights


