import scipy.optimize
import numpy as np
import time

class struct_svm_solver_bundle_scipy:
    """
    A solver to a struct SVM problem, formulated in the same way as for dlib.
    To reproduce the call "weights = dlib.solve_structural_svm_problem(problem)",
    create an instance of this class and call solve(), which returns the weights.

    Using scipy.optimize for the QP

    @author: Carsten Haubold
    """

    def __init__(self, problem):
        """
        """

        # parameters of the SSVM problem
        self.__epsilon = 0.001
        self.__problem = problem
        try:
            self.__epsilon = problem.epsilon
        except:
            print("Epsilon not set in given struct svm problem, using 0.001")

        # hold all constraints of the optimization problem
        self.__constraints = []

        # nonnegative weights?
        self.__learn_nonnegative_weights = True
        try:
            self.__learn_nonnegative_weights = problem.learns_nonnegative_weights
        except:
            print("Problem does not specify whether non-negative weights should be learned. Setting to True.")

        if self.__learn_nonnegative_weights:
            print("Setting up constraints to prevent weights from becoming negative")
            for i in range(self.__problem.num_dimensions):
                # Note the argument i with default value below.
                # This is needed because otherwise all functions created in the loop will reference the same i,
                # leading to all of them being exactly the same value
                def constraint_function(weights, i=i):
                    print("Constraint for weight {} value: {}".format(i, weights))
                    return np.array([weights[i]])

                def constraint_gradient(weights, i=i):
                    grad = np.zeros_like(weights)
                    grad[i] = 1.0
                    print("Gradient for weight {}:  {}".format(i, grad))
                    return grad

                self.__constraints.append({'type':'ineq', 'fun': constraint_function, 'jac': constraint_gradient })
        else:
            print("Weights are allowed to be negative")

    def solve(self):
        """
        Solve with the bundle method as implemented in sbmrm
        """

        # setup QP functions:
        def objective_function(x):
            weights = x[:-1]
            slack = x[-1]
            print("objective value: {}".format(0.5 * self.__problem.C * np.sum([w * w for w in weights]) + slack))
            return np.array([0.5 * self.__problem.C * np.sum([w * w for w in weights]) + slack])

        def objective_gradient(x):
            # Jacobi matrix
            weights = x[:-1]
            slack = x[-1]
            jacobi_entries = []
            for w in weights:
                dxdw = self.__problem.C * w
                jacobi_entries.append(dxdw)
            jacobi_entries.append(1.0) # gradient for slack
            print("objective gradient: {}".format(jacobi_entries))
            return np.array(jacobi_entries)

        # run bundle method
        not_converged = True
        num_iterations = 0
        current_weights = np.zeros(self.__problem.num_dimensions) # initial values are zero for now
        slack = 0.0
        print("Initial values: weights={}, slack={}".format(current_weights, slack))
        min_value = np.inf
        start_time = time.time()

        while not_converged:
            num_iterations += 1

            # get value and gradient for current weights
            value, gradient = self.__problem.get_value_and_gradient(current_weights)
            min_value = min(value - 0.5 * self.__problem.C * np.dot(current_weights, current_weights), min_value)
            print("Gradient is {}".format(gradient))
            print("Min L(w) is {}".format(min_value))

            # construct hyperplane (ax + b) for bundle
            a = gradient
            b = value - np.dot(current_weights, a)

            print("Adding hyperplane {} * w + {}".format(np.array(a), b))

            def hyperplane_func(x, a=a, b=b, it=num_iterations):
                weights = x[:-1]
                slack = x[-1]
                print("hyperplane ({}) value: {}".format(it, -1.0 * np.dot(weights, a) + slack - b))
                return np.array([-1.0 * np.dot(weights, a) + slack - b])

            def hyperplane_gradient(x, a=a, b=b, it=num_iterations):
                jacobi_entries = []
                for i in range(len(x)-1):
                    dxdw = -1.0 * a[i]
                    jacobi_entries.append(dxdw)
                jacobi_entries.append(1.0) # gradient for slack
                print("hyperplane ({}) gradient: {}".format(it, jacobi_entries))
                return np.array(jacobi_entries)
            
            # add hyperplane as constraint to model
            hyperplane = {'type': 'ineq', 'fun': hyperplane_func, 'jac': hyperplane_gradient}
            self.__constraints.append(hyperplane)
            # self.__model.addConstr(np.dot(self.__weights, a) - self.__slack <= -1.0 * b)
            print("subj. to new constraint: {} <= {}".format(np.dot(current_weights, a) - slack, -1.0 * b))

            # solve current QP and extract current_weights and slack
            init_value = np.hstack([current_weights, np.array(slack)])
            print("Starting from {}".format(init_value))
            result = scipy.optimize.minimize(objective_function, init_value, jac=objective_gradient, method='SLSQP', constraints=self.__constraints, options={'disp': True})
            assert(result.success)
            current_weights = result.x[:-1]
            slack = result.x[-1]
            print("Optimization found weights {} and slack {}".format(current_weights, slack))

            min_lower = result.fun

            eps = min_value - min_lower
            # check exit condition:
            if eps > self.__epsilon:
                not_converged = True
            else:
                not_converged = False

        print("Number of iterations: {}".format(num_iterations))
        end_time = time.time()
        print("Done in {} secs".format(end_time - start_time))

        return current_weights


