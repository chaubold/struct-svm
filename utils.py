
def hamming_loss(labels_a, labels_b):
    # Compute the hamming loss for two given solutions
    """
    compute hamming loss for two labelings
    :param labels_a:
    :param labels_b:
    """
    # print("Computing Hamming Loss for labels {} and {}".format(np.array(labels_a), np.array(labels_b)))
    return sum([abs(a - b) for a, b in zip(labels_a, labels_b)])


def weighted_hamming_loss(labels_a, labels_b, label_cost):
    # Compute the hamming loss for two given solutions
    """
    compute weighted hamming loss for two labelings = dot(abs(labels_a,labels_b), variable_class_weight)
    :param labels_a:
    :param labels_b:
    """
    # print("Computing Hamming Loss for labels {} and {}".format(np.array(labels_a), np.array(labels_b)))
    return sum([c * abs(a - b) for a, b, c in zip(labels_a, labels_b, label_cost)])


def multiclass_weighted_hamming_loss(labels_a, labels_b, label_class_multiplier_list, label_class_cost):
    # Compute the hamming loss for two given solutions
    """
    compute weighted hamming loss for two labelings = dot(abs(labels_a,labels_b),
    where each label can be a weighted combination of classes
    :param labels_a:
    :param labels_b:
    :param label_class_multiplier_list: list of (class, multiplier) tuples per label
    :param label_class_cost: cost per class
    """
    loss = 0

    for a, b, cm_list in zip(labels_a, labels_b, label_class_multiplier_list):
        if a != b:
            for c, m in cm_list:
                loss += label_class_cost[c] * m * abs(a - b)

    return loss


def remove_comments_from_line(line):
    line = line.strip()

    # cut of comment at end
    if line.find('#') > -1:
        line = line[0:line.find('#')]

    return line.strip()


def load_labelfile_with_classes_and_multipliers(filename):
    """
    Loads the ground truth labeling y' from a file of the format:

    labels.txt:

      0 #c0\t # one digit per component of y', optionally followed by a series of classes #c1:4 where the class is '1' and '4' is the multiplier for that class
      1 # class 0 with multiplier 1 is assumed if none given
      0
      1
    """
    with open(filename, 'rt') as label_file:
        label = []
        label_class_multiplier_list = []

        label_classes = set()
        label_classes.add(0) # default

        for line in label_file:
            # find class configuration starting with '#c' followed by int
            cm_list = []
            start_idx = 0
            linelist = line.split()

            for elem in linelist[1:]:
                elem.strip()
                if elem.startswith('#c'):
                    label_class = int(elem[2:elem.index(':')])
                    if ':' in elem:
                        multiplier = int(elem[elem.index(':') + 1:])
                    else:
                        multiplier = 1
                    cm_list.append((label_class, multiplier))
                    label_classes.add(label_class)

            line = remove_comments_from_line(line)

            # ignore if line is a comment or empty
            if len(line) == 0 or line[0] == '#':
                continue

            # add to labeling vector
            label.append(int(line))
            if len(cm_list) == 0:
                cm_list = [(0, 1)]
            label_class_multiplier_list.append(cm_list)

    return label, label_class_multiplier_list, label_classes


def load_proposal_labeling(proposal_filename):
    proposal_labeling = []
    with open(proposal_filename, 'rt') as proposal_file:
        for full_line in proposal_file:
            line = remove_comments_from_line(full_line)

            # ignore if line is a comment or empty
            if len(line) == 0 or line[0] == '#':
                continue

            try:
                proposal_labeling.append(int(line))
            except:
                try:
                    proposal_labeling.append(int(float(line)))
                    print("Downcasting {} to {}".format(line, int(float(line))))
                except:
                    print("Problems while parsing line: " + full_line)

    print("\tFound proposal of length {}".format(len(proposal_labeling)))

    return proposal_labeling


def normalize_feature_matrix(feature_matrix):
    """
    In-place normalization of the given feature matrix.
    :param feature_matrix: Numpy array, each row is a feature, columns are samples
    :return: mean and variance used to normalize each feature
    """
    import numpy as np

    num_features = feature_matrix.shape[1]
    means = np.zeros(num_features)
    variances = np.zeros(num_features)

    for f in range(num_features):
        # get mean and variance
        feats = feature_matrix[:, f]
        m = np.mean(feats)
        v = np.var(feats)

        # normalize
        if v > 0.0:
            feature_matrix[:, f] = (feats - m) / v

        # store
        means[f] = m
        variances[f] = v

    return means, variances


def apply_feature_normalization(feature_matrix, means, variances):
    """
    In-place normalization of the given feature matrix.
    :param feature_matrix: Numpy array, each row is a feature, columns are samples
    :param means: per-feature mean
    :param variances: per-feature variance
    """
    import numpy as np

    num_features = feature_matrix.shape[1]

    for f in range(num_features):
        feats = feature_matrix[:, f]
        v = variances[f]
        m = means[f]

        if v > 0.0:
            feature_matrix[:, f] = (feats - m) / v