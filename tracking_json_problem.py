from struct_svm_solver_bundle import *
from struct_svm_solver_bundle_scipy import *

import numpy as np
import argparse
import time
import utils
import json
import copy
from pprint import pprint

# ---------------------------------------------------------------------------------------
# helper methods copied from hyra.core.jsongraph
# ---------------------------------------------------------------------------------------
def listify(l):
    ''' put every element of the list in it's own list, and thus extends the depth of nested lists by one '''
    return [[e] for e in l]

def delistify(l):
    ''' take every element out of it's own list '''
    return [e[0] for e in l]

def checkForConvexity(feats):
    ''' check whether the given array of numbers is convex, meaning that the difference between consecutive numbers never decreases '''
    grad = feats[1:] - feats[0:-1]
    for i in range(len(grad) - 1):
        assert(grad[i+1] > grad[i])

def convexify(featureList, eps, column=0):
    ''' 
    convexify the given column of the provided features 

    Note: adjusted for multiple columns compared to the hytra version!
    '''
    originalColumns = zip(*featureList)
    listOfNumbers = originalColumns[column]
    features = np.array(listOfNumbers)
    if len(features.shape) != 1:
        raise ValueError('This script can only convexify feature vectors with one feature per state!')

    # Note from Numpy Docs: In case of multiple occurrences of the minimum values, the indices corresponding to the first occurrence are returned.
    bestState = np.argmin(features)

    for direction in [-1, 1]:
        pos = bestState + direction
        previousGradient = 0
        while pos >= 0 and pos < features.shape[0]:
            newGradient = features[pos] - features[pos-direction]
            if np.abs(newGradient - previousGradient) < eps:
                # cost function's derivative is roughly constant, add epsilon
                previousGradient += eps
                features[pos] = features[pos-direction] + previousGradient
            elif newGradient < previousGradient:
                # cost function got too flat, set feature value to match old slope
                previousGradient += eps
                features[pos] = features[pos-direction] + previousGradient
            else:
                # all good, continue with new slope
                previousGradient = newGradient

            pos += direction
    try:
        checkForConvexity(features)
    except:
        print("Failed convexifying {}".format(features))

    originalColumns[column] = features.flatten()

    return [list(r) for r in zip(*originalColumns)]

# ---------------------------------------------------------------------------------------
# tracking SSVM problem that can be solved by the bundle solvers in this repo
# ---------------------------------------------------------------------------------------
class tracking_json_problem(object):
    C = 1
    be_verbose = True
    learns_nonnegative_weights = True
    # learns_nonnegative_weights = False
    epsilon = 0.001

    def __init__(self,
                 model_filename,
                 gt_filename,
                 tracker):
        '''
        Construct a tracking problem that reads the model from `model_filename`, 
        the groundtruth from `gt_filename`, and uses the method `result = tracker(model, weights)`.
        '''
        
        # load data into the model
        self._model = self._load_model(model_filename)
        self._gt = self._load_ground_truth(gt_filename)
        self._tracker = tracker

        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        print("Got Model")
        pprint(self._model)
        print("and gt")
        pprint(self._gt)
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

        # set parameters read from the solver:
        self.num_samples = 1
        self.num_dimensions = self._get_num_weights()

    def get_value_and_gradient(self, current_weights):
        """
        Callback for bundle method
        :param current_weights:
        :return:
        """
        solution, value = self.separation_oracle(0, current_weights)
        return value, self._get_gradient(solution)

    def _get_gradient(self, solution):
        temp_weights = np.zeros(self.num_dimensions)
        gradient = np.zeros(self.num_dimensions)

        for idx in range(self.num_dimensions):
            temp_weights[idx] = 1.0
            gradient[idx] = self._evaluate_solution(self._model, self._gt, temp_weights) - self._evaluate_solution(self._model, solution, temp_weights)
            temp_weights[idx] = 0.0
        return gradient
        # return np.dot(self._ground_truth_label, self._feature_matrix) - np.dot(solution, self._feature_matrix)

    def separation_oracle(self, idx, current_weights):
        """
        Then the separation oracle finds a Y such that:
            Y = argmax over all y: LOSS(y_gt,y) + F(X,y_gt) - F(X,y) 
        or equivalently:
            Y = argmin over all y: F(X,y) - LOSS(y_gt,y) - F(X,y_gt) 

        Finally, separation_oracle() returns Y, and its value of the above equation
        """
        assert(idx == 0)
        print("\n========================================\n\nSeparation Oracle called with weights: {}".format(current_weights))
        start_time = time.time()

        current_weights_lut = {'link': current_weights[0], 'det': current_weights[1]}
        if len(current_weights) == 5:
            current_weights_lut['div'] = current_weights[2]
            current_weights_lut['app'] = current_weights[3]
            current_weights_lut['dis'] = current_weights[4]
        else:
            current_weights_lut['app'] = current_weights[2]
            current_weights_lut['dis'] = current_weights[3]
        loss_augmented_model, constant_loss_term = self._build_loss_augmented_model(current_weights_lut)

        # minimize
        print("\toptimizing loss augmented model")
        # the loss augmented model already performed the weight multiplication of the feature, 
        # thus we only use ones as weights for tracking and evaluation
        loss_augmented_weights = [1.0] * len(current_weights)

        weights_dict = {'weights': list(loss_augmented_weights)}
        result = self._tracker(loss_augmented_model, weights_dict)
        
        # extract solution
        result_dict = self._result_to_dictionary(result)
        value = self._evaluate_solution(loss_augmented_model, result_dict, loss_augmented_weights) - constant_loss_term - self._evaluate_solution(self._model, self._gt, current_weights)
        
        print("using loss augmented weights: {}".format(loss_augmented_weights))
        print("Constant loss term: {}".format(constant_loss_term))
        print("Evaluating loss augmented - gt: {}".format(value))

        # timing
        end_time = time.time()
        print("\tSeparation oracle took {} secs\n\n========================================\n".format(end_time - start_time))

        return result_dict, value

    @staticmethod
    def _evaluate_solution(model, result_dict, weights):
        ''' 
        evaluate the given model with the specified weights by summing over feature*weight products of the selected states
        '''
        value = 0.0
        if len(weights) > 5:
            weight_stride = 2
        else:
            weight_stride = 1
        
        for l in model['linkingHypotheses']:
            feats = l['features']
            result_state = result_dict['link'].get((l['src'], l['dest']), 0) # variables have state 0 if they are not listed in the GT
            value += np.dot(feats[result_state], weights[0:weight_stride])

        with_divisions = False
        for s in model['segmentationHypotheses']:
            if 'divisionFeatures' in s:
                with_divisions = True
                break

        if with_divisions:
            featNames = ['features', 'divisionFeatures', 'appearanceFeatures', 'disappearanceFeatures']
            resultGroups = ['det', 'div', 'app', 'dis']
            offsets = [1, 2, 3, 4]
        else:
            featNames = ['features', 'appearanceFeatures', 'disappearanceFeatures']
            resultGroups = ['det', 'app', 'dis']
            offsets = [1, 2, 3]

        for s in model['segmentationHypotheses']:
            for f, t, o in zip(featNames, resultGroups, offsets):
                feats = s[f]
                result_state = result_dict[t].get(s['id'], 0) # variables have state 0 if they are not listed in the GT
                value += np.dot(feats[result_state], weights[o * weight_stride:(o + 1) * weight_stride])

        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        pprint(result_dict)
        print("Value of solution with weight {} is {}".format(weights, value))
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

        return value

    def _get_num_weights(self):
        ''' Assuming that there is only one weight per event (det/div/link/app/dis), we check which of those are present in the GT and return the number '''
        num_weights = 0

        featNames = ['features', 'divisionFeatures', 'appearanceFeatures', 'disappearanceFeatures']
        for f in featNames:
            if any(f in s for s in self._model['segmentationHypotheses']):
                num_weights += 1

        if any('features' in l for l in self._model['linkingHypotheses']):
            num_weights += 1

        return num_weights

    def _result_to_dictionary(self, result):
        '''
        transform result into dictionaries indexed by variable ID for detection and division, and tuples of IDs for links
        '''

        # mask whether an incoming/outgoing connection is needed for this variable, and if that is not found,
        # we emplace appearance/disappearances
        incoming_mask = {}
        outgoing_mask = {}

        result_detections = {}
        for d in result['detectionResults']:
            result_detections[d['id']] = d['value']
            if d['value'] > 0:
                incoming_mask[d['id']] = True
                outgoing_mask[d['id']] = True

        result_divisions = {}
        for d in result['divisionResults']:
            result_divisions[d['id']] = d['value']

        result_links = {}
        for l in result['linkingResults']:
            result_links[(l['src'], l['dest'])] = l['value']
            assert l['src'] in outgoing_mask or l['value'] == 0
            assert l['dest'] in incoming_mask or l['value'] == 0
            outgoing_mask[l['src']] = False
            incoming_mask[l['dest']] = False

        result_appearances = {}
        for detId, notCoveredByLinks in incoming_mask.iteritems():
            if notCoveredByLinks:
                result_appearances[detId] = 1

        result_disappearances = {}
        for detId, notCoveredByLinks in outgoing_mask.iteritems():
            if notCoveredByLinks:
                result_disappearances[detId] = 1

        return {'det':result_detections, 'div':result_divisions, 'link':result_links, 'app':result_appearances, 'dis':result_disappearances}

    def _build_loss_augmented_model(self, current_weights_lut):
        """
        Return a copy of the original model with a constant feature added to every variable state, indicating the loss attached to that state.
        It is +1 for non-matching labels, and -1 for matching ones. The second return value is a constant term to 
        add to the objective that makes sure that the loss becomes zero when the optimal solution is obtained.
        """

        loss_augmented_model = copy.deepcopy(self._model)

        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        print("Model before augmentation:")
        pprint(self._model)
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

        detection_feature_result_mapping = {'features':'det', 'divisionFeatures':'div', 'appearanceFeatures':'app', 'disappearanceFeatures':'dis'}

        for l in loss_augmented_model['linkingHypotheses']:
            feats = l['features']
            gt_state = self._gt['link'].get((l['src'], l['dest']), 0) # variables have state 0 if they are not listed in the GT
            for state_idx, state_feats in enumerate(feats):
                if state_idx == gt_state:
                    loss_value = -1.0
                else:
                    loss_value = 1.0
                
                state_feats[0] = state_feats[0] * current_weights_lut['link'] - loss_value

        for s in loss_augmented_model['segmentationHypotheses']:
            for f in ['features', 'divisionFeatures', 'appearanceFeatures', 'disappearanceFeatures']:
                if f in s:
                    feats = s[f]
                    result_type = detection_feature_result_mapping[f]
                    gt_state = self._gt[result_type].get(s['id'], 0) # variables have state 0 if they are not listed in the GT
                    for state_idx, state_feats in enumerate(feats):
                        if state_idx == gt_state:
                            loss_value = -1.0
                        else:
                            loss_value = 1.0
                        
                        state_feats[0] = state_feats[0] * current_weights_lut[result_type] - loss_value

        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        print("Model after augmentation but before convexification:")
        pprint(loss_augmented_model)
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

        # convexify combined values
        # loss_augmented_model = self.convexify(loss_augmented_model)

        # collect constant loss term now that we've fitted a convex envelope
        constant_term = 0.0


        for loss_l, original_l in zip(loss_augmented_model['linkingHypotheses'], self._model['linkingHypotheses']):
            assert loss_l['src'] == original_l['src']
            assert loss_l['dest'] == original_l['dest']
            feats = original_l['features']
            gt_state = self._gt['link'].get((original_l['src'], original_l['dest']), 0)
            constant_term += original_l['features'][gt_state][0] * current_weights_lut['link'] - loss_l['features'][gt_state][0]

        for loss_s, original_s in zip(loss_augmented_model['segmentationHypotheses'], self._model['segmentationHypotheses']):
            assert loss_s['id'] == original_s['id']
            for f in ['features', 'divisionFeatures', 'appearanceFeatures', 'disappearanceFeatures']:
                if f in loss_s:
                    result_type = detection_feature_result_mapping[f]
                    gt_state = self._gt[result_type].get(loss_s['id'], 0) # variables have state 0 if they are not listed in the GT
                    constant_term += original_s[f][gt_state][0] * current_weights_lut[result_type] - loss_s[f][gt_state][0]

        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        print("Model after augmentation and convexification:")
        pprint(loss_augmented_model)
        print("Constant loss term {}".format(constant_term))
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

        return loss_augmented_model, constant_term

    @staticmethod
    def convexify(model, epsilon=0.000001):
        '''
        Convexify all cost vectors in this model (in place!).
        If two values are equal, the specified `epsilon` will be added to make sure the gradient
        does not stay at 0.

        Needed to run the flow solver afterwards.

        Note: taken and adjusted from hytra.core.jsongraph
        '''
        if not model['settings']['statesShareWeights']:
            raise ValueError('This script can only convexify feature vectors with shared weights!')

        if 'segmentationHypotheses' in model:
            segmentationHypotheses = model['segmentationHypotheses']
        else:
            segmentationHypotheses = []

        if 'linkingHypotheses' in model:
            linkingHypotheses = model['linkingHypotheses']
        else:
            linkingHypotheses = []

        if 'divisionHypotheses' in model:
            divisionHypotheses = model['divisionHypotheses']
        else:
            divisionHypotheses = []

        for seg in segmentationHypotheses:
            for f in ['features', 'appearanceFeatures', 'disappearanceFeatures']:
                if f in seg:
                    try:
                        seg[f] = convexify(seg[f], epsilon, column=0)
                    except:
                        print("Convexification failed for feature {} of :{}".format(f, seg))
                        exit(0)
            # division features are always convex (2 values defines just a line)

        for link in linkingHypotheses:
            link['features'] = convexify(link['features'], epsilon, column=0)

        for division in divisionHypotheses:
            division['features'] = convexify(division['features'], epsilon, column=0)

        return model


    def _load_model(self, model_filename):
        """
        Loads the tracking model from a JSON file
        """
        with open(model_filename, 'rt') as model_file:
            return json.load(model_file)

    def _load_ground_truth(self, gt_filename):
        """
        Loads the ground truth labeling from a JSON file
        """
        with open(gt_filename, 'rt') as gt_file:
            gt =  json.load(gt_file)

        return self._result_to_dictionary(gt)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="structured learning for conservation tracking problems specified in hytra's JSON format")
    parser.add_argument('--model', dest='model_filename', type=str, required=True, help='Filename for the model')
    parser.add_argument('--gt', dest='gt_filename', type=str, required=True, help='Filename for ground truth labelling')
    # parser.add_argument('--loss_weights', metavar='loss_weights', type=float, nargs='+', help="Weight loss of labels depending on their class, all the same if not specified.")
    parser.add_argument('--output', dest='weights_filename', type=str, required=True, help='Weights json filename')
    args = parser.parse_args()

    # import dpct
    # tracker = dpct.trackFlowBased
    import multiHypoTracking_with_cplex as mht
    tracker = mht.track

    problem = tracking_json_problem(args.model_filename, args.gt_filename, tracker)
        
    solver = struct_svm_solver_bundle(problem)
    # solver = struct_svm_solver_bundle_scipy(problem)
    weights = solver.solve()

    print("Found weights: {}".format(weights))
    outfile = file(args.weights_filename, 'wt')
    json.dump({'weights':list(weights)}, outfile)
    outfile.close()
    print("Saved weights to " + args.weights_filename)

