import gurobipy

class struct_svm_solver_primal:
    """
    A solver to a struct SVM problem, formulated in the same way as for dlib.
    To reproduce the call "weights = dlib.solve_structural_svm_problem(problem)",
    create an instance of this class and call solve(), which returns the weights.

    It is not intended to be fast, but to serve as example implementation.

    @author: Carsten Haubold
    """

    def __init__(self, problem):
        """
        """

        # parameters of the SSVM problem
        self.__problem = problem

        # Gurobi model for the n-slack svm
        self.__model = gurobipy.Model("struct_svm")
        self.__weights = []
        self.__slacks = []
        self.__y_bests = []

        # nonnegative weights?
        learn_nonnegative_weights = True
        try:
            learn_nonnegative_weights = problem.learns_nonnegative_weights
        except:
            print("Problem does not specify whether non-negative weights should be learned. Setting to True.")

        # add variables to optimization problem
        for i in range(self.__problem.num_dimensions):
            if learn_nonnegative_weights:
                self.__weights.append(self.__model.addVar())  # by default variables are continuous in [0,inf)
            else:
                self.__weights.append(self.__model.addVar(lb=-gurobipy.GRB.INFINITY))

        for i in range(self.__problem.num_samples):
            self.__slacks.append(self.__model.addVar())
            self.__y_bests.append(self.__problem.get_best_proposal_for_sample(i))

        # Integrate new variables
        self.__model.update()

        # add constraints
        for i in range(self.__problem.num_samples):
            for y in range(self.__problem.get_num_proposals_for_sample(i)):
                if y == self.__y_bests[i]:  # skip best y in proposals
                    continue
                delta_psi = self.__problem.get_proposal_feature_vector(i, self.__y_bests[i]) \
                            - self.__problem.get_proposal_feature_vector(i, y)
                loss_y = self.__problem.get_proposal_loss(i, y) - self.__problem.get_proposal_loss(i, self.__y_bests[i])
                if loss_y > 0:  # only include constraint if they have different loss
                    self.__model.addConstr(sum([self.__weights[c] * delta_psi[c] for c in range(len(self.__weights))])
                                           >= 1.0 - self.__slacks[i] / loss_y)

        self.__model.update()

        # Set objective
        obj = gurobipy.QuadExpr()
        
        for w_i in self.__weights:
            obj += w_i * w_i

        for xsi_i in self.__slacks:
            obj += self.__problem.C * xsi_i

        self.__model.setObjective(obj, gurobipy.GRB.MINIMIZE)
        # Use the following setting if the problem seems to be unbounded or infeasible.
        # self.__model.Params.BarHomogeneous = 1

    def solve(self):
        """
        Solves the quadratic problem using gurobi, 
        instanciating all constraints given by the diverse proposals
        """
        self.__model.optimize()

        weights = [w.getAttr(gurobipy.GRB.attr.X) for w in self.__weights]
        return weights