"""
Struct SVM Solver based on gurobi, with similar interface as SBMRM by Jan Funke.

===========
= Problems =
===========

== struct_svm_ranking_problem == 
Reads proposal features, proposal labelings, and ground truth labeling from text files.
Learns which proposal is closest to ground truth based on some measure (e.g. hamming loss). Solve with primal solver!

== funkey_problem ==
Exposes the exact same interface as SBMRM, reading the files labels.txt,
features.txt and constraints.txt. Solve with dual solver!

===========
= Solvers =
===========

== struct_svm_solver_dual ==
Solver for standard struct svm problems, which requires you to formulate your problem
in exactly the same way as dlib does. Needs a separation oracle.

== struct_svm_solver_primal ==
Solver for the ranking problems which does not perform any cutting plane approach.
All constraints are added right away, as they depend only on the number proposals.

"""

try:
    from struct_svm_ranking_problem import struct_svm_ranking_problem
    from funkey_problem import funkey_problem
    from funkey_struct_svm_ranking_problem import funkey_struct_svm_ranking_problem
    from struct_svm_solver_dual import struct_svm_solver_dual
    from struct_svm_solver_primal import struct_svm_solver_primal
    from struct_svm_solver_1_slack import struct_svm_solver_1_slack
    from struct_svm_solver_bundle import struct_svm_solver_bundle
except Exception as e:
    print("Warning, errors occurred when importing StructSVM problems and solvers: {}".format(e))

