import gurobipy
import numpy as np
import time

class struct_svm_solver_bundle:
    """
    A solver to a struct SVM problem, formulated in the same way as for dlib.
    To reproduce the call "weights = dlib.solve_structural_svm_problem(problem)",
    create an instance of this class and call solve(), which returns the weights.

    It is not intended to be fast, but to serve as example implementation.

    @author: Carsten Haubold
    """

    def __init__(self, problem):
        """
        """

        # parameters of the SSVM problem
        self.__epsilon = 0.001
        self.__problem = problem
        try:
            self.__epsilon = problem.epsilon
        except:
            print("Epsilon not set in given struct svm problem, using 0.001")

        # nonnegative weights?
        self.__learn_nonnegative_weights = True
        try:
            self.__learn_nonnegative_weights = problem.learns_nonnegative_weights
        except:
            print("Problem does not specify whether non-negative weights should be learned. Setting to True.")



        # Gurobi model for the 1-slack svm
        self.__model = gurobipy.Model("bundle_struct_svm")
        
        # add variables to optimization problem
        self.__weights = []
        for i in range(self.__problem.num_dimensions):
            if self.__learn_nonnegative_weights:
                self.__weights.append(self.__model.addVar())  # by default variables are continuous in [0,inf)

            else:
                self.__weights.append(self.__model.addVar(lb=-gurobipy.GRB.INFINITY))

        self.__slack = self.__model.addVar()

        # setup QP:
        self.__model.update()
        obj = gurobipy.QuadExpr()

        for w_i in self.__weights:
            obj += w_i * w_i

        self.__model.setObjective(0.5 * self.__problem.C * obj + self.__slack, gurobipy.GRB.MINIMIZE)

    def solve(self):
        """
        Solve with the bundle method as implemented in sbmrm
        """
        not_converged = True
        num_iterations = 0
        weights = np.zeros(self.__problem.num_dimensions)
        min_value = np.inf
        start_time = time.time()

        while not_converged:
            num_iterations += 1

            # get value and gradient for current weights
            value, gradient = self.__problem.get_value_and_gradient(weights)
            min_value = min(value - 0.5 * self.__problem.C * np.dot(weights, weights), min_value)
            print("Gradient is {}".format(gradient))
            print("Min L(w) is {}".format(min_value))

            # construct hyperplane (ax + b) for bundle
            a = gradient
            b = value - np.dot(weights, a)

            print("Adding hyperplane {} * w + {}".format(np.array(a), b))

            # add hyperplane as constraint to model
            self.__model.addConstr(np.dot(self.__weights, a) - self.__slack <= -1.0 * b)
            print("subj. to new constraint: {} <= {}".format(np.dot(self.__weights, a) - self.__slack, -1.0 * b))
            self.__model.update()

            # solve current QP and extract weights and slack
            print self.__model.getObjective()
            self.__model.optimize()
            weights = [wi.getAttr(gurobipy.GRB.attr.X) for wi in self.__weights]
            slack = self.__slack.getAttr(gurobipy.GRB.attr.X)
            print("Optimization found weights {} and slack {}".format(weights, slack))

            min_lower = self.__model.objVal

            eps = min_value - min_lower
            # check exit condition:
            if eps > self.__epsilon:
                not_converged = True
            else:
                not_converged = False

        print("Number of iterations: {}".format(num_iterations))
        end_time = time.time()
        print("Done in {} secs".format(end_time - start_time))

        return weights


