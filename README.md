# Structured SVM playground #

This is a python playground using dlib, gurobipy and opengm to explore structured svms and their solvers.

There are three examples: `tracking_struct_svm_test` and `dlib_struct_svm_example` which can be run using the dlib, the dual-, and 1-slack solver implemented here. The last example is `tracking_struct_svm_ranking_test` which presents some proposals to the solver which is supposed to learn the compatibility function to mirror their ordering by loss. The last one is solved by `struct_svm_solver_primal`.

A set of solvers are developed here:

 * `struct_svm_solver_dual` solves exactly the same problem as the one from dlib (`dlib.solve_structural_svm_problem()`), and solves the dual problem by iteratively adding cutting planes for most violated constraints, found through a separation oracle. It follows exactly Algorithm 1 for the n-slack SVM with slack rescaling in the paper *LARGE MARGIN METHODS FOR STRUCTURED AND INTERDEPENDENT OUTPUT VARIABLES* by Tsochantaridis.
* `struct_svm_solver_1_slack` is an implementation of *Cutting Plane Training of Structural SVMs* by Joachims
* `struct_svm_solver_bundle` implements *Bundle Methods for Regularized Risk Minimization* by Teo, which is used by the `funkey_problem`, and is configured exactly in the same way as Jan Funke's SBMRM. It requires the problem to provide a `get_value_and_gradient` method for a given set of weights, and assumes you only have one positive example.
 * `struct_svm_solver_primal` is meant to work for diverse-m-best reranking via structured learning (See *Discriminative Re-ranking of Diverse Segmentations* by Yadollahpur), where one does not need to search for the most violated constraint in a potentially infinitely large space of negative examples, but the set of examples is limited to the diverse proposals gained in a previous step of the pipeline.

To be compatible with Funkey's Structured Learning framework [SBMRM](http://github.com/funkey/sbmrm), there is  `funkey_problem.py` which uses the exact same input (files `constraints.txt`, `labels.txt`, and `features.txt` as documented in the code). It builds the objective from the given constraints and features, and sets up a structured learning problem such that in the separation oracle the loss-augmented objective is minimized. 

## Installation instructions ##
Clone this repository into your `site-packages` path, or in any folder that is in your `PYTHONPATH`, or put a symlink there.
Make sure to call your clone `structsvm`, Python does not like dashes in module names which I did not know when I set up that repository. Like so:
```
git clone https://bitbucket.org/chaubold/struct-svm.git structsvm
```

## How to set up your own problem ##
All solvers require you to set up your problem in a form that is similar (or the same) as for the dlib solver.
```py
class my_problem:
    def __init__(self, samples, labels_ground_truth, labels_proposals, edges):
        # required fields are:
        
        # define SVM regularization strength
        self.C = 1
        
        # only used by dlib, the others are verbose by default
        self.be_verbose = True 

        # set whether the learned weights are allowed to be negative (not used by all solvers)
        self.learns_nonnegative_weights = True
        
        # the number of positive training samples (1 is usually enough, funkey only uses one)
        self.num_samples = len(samples)
        
        # the dimensionality of the weight vector
        self.num_dimensions = 2
        
        # up to which precision the SVM is trained. optional, default is 0.0001
        self.epsilon = 0.001
    
    # return PSI( samples[idx], ground_truth[idx] )    
    def get_truth_joint_feature_vector(self, idx):
        """
        In get_truth_joint_feature_vector(), all you have to do is return the PSI() vector
        for the idx-th training sample when it has its true label.  So here it returns
        PSI(self.samples[idx], self.labels[idx]).
        """

    # find most violated constraint
    def separation_oracle(self, idx, current_solution):
        """
        The separation oracle finds a Y such that:
            Y = argmax over all y: LOSS(idx,y) + F(X,y) - F(X,idx)
                                                        ^^^^^^^^^^^
                                                       only if using bundle solver

            (i.e. It finds the label which maximizes the above expression.)

        Finally, separation_oracle() returns LOSS(idx,Y),PSI(X,Y)
        """
```

The problem can then be solved as follows:
```py
from my_problem import *
import structsvm
problem = my_problem()

# any of the following:
solver = struct_svm_solver_dual(problem)
solver = struct_svm_solver_1_slack(problem)
solver = struct_svm_solver_bundle(problem)
weights = solver.solve()

# or, if dlib is present:
import dlib
weights = dlib.solve_structural_svm_problem(problem)
```

See the [DLib example](http://dlib.net/svm_struct.py.html) for a more in depth explanation.

## Prerequisites ##
* [Gurobi](http://www.gurobi.com) needs to be installed, licensed (free for academic use), and available in Python. If you are using a self-compiled version of python, it might be best to copy the `gurobipy` folder from your system folder to a local python `site-packages` and point gurobipy to the proper libpythonXX.so (check `ldd gurobipy.so`). Crashes on importing gurobipy indicate that this needs to be done.

* Optional: build the dlib python wrapper (see header of `dlib_struct_svm_example`) and put the lib (or a symlink) into your `site-packages`

## Notes ##
* **dlib** includes a python wrapper, and from version 18.10 on, also the struct svm example for python which was formerly only available in C++. The `struct_svm_solver_dual` implemented here uses the same interface, and yields compareable results to the dlib solver. **Optional!**
* **gurobipy** is used to solve the QP minimization in the solver
* the `tracking_struct_svm_test` constructs a small graphical model, using the to-be-learned weights in the unaries and pairwise potentials. **OpenGM** is used for this GM, and to perform inference on it. **Optional!**