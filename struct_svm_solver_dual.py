import gurobipy
import numpy as np
import time

class struct_svm_solver_dual:
    """
    A solver to a struct SVM problem, formulated in the same way as for dlib.
    To reproduce the call "weights = dlib.solve_structural_svm_problem(problem)",
    create an instance of this class and call solve(), which returns the weights.

    It is not intended to be fast, but to serve as example implementation.

    @author: Carsten Haubold
    """

    def __init__(self, problem):
        """
        """

        # parameters of the SSVM problem
        self.__problem = problem
        try:
            self.__epsilon = problem.epsilon
        except:
            print("Epsilon not set in given struct svm problem, using 0.001")

        # nonnegative weights?
        # TODO: use this flag!
        self.__learn_nonnegative_weights = True
        try:
            self.__learn_nonnegative_weights = problem.learns_nonnegative_weights
        except:
            print("Problem does not specify whether non-negative weights should be learned. Setting to True.")

        # Gurobi model for the n-slack svm
        self.__model = gurobipy.Model("struct_svm")
        self.__weights = np.zeros(problem.num_dimensions)
        self.__slacks = []
        self.__dual_vars = {}
        self.__epsilon = 0.001
        self.__constraints_per_sample = {}
        self.__gurobi_constraints_per_sample = {}
        self.__psi_gt = [problem.get_truth_joint_feature_vector(i) for i in range(problem.num_samples)]

        # initially the QP already contains dual vars for the samples
        for i in range(self.__problem.num_samples):
            for psi, nr in self.__all_y_without_i(i):
                self.__dual_vars[(i, nr)] = self.__model.addVar()
        self.__model.update()

    def __compute_H_y(self, psi_gt, psi_y, loss_y):
        return loss_y - np.dot(self.__weights, np.array(psi_gt) - np.array(psi_y))

    def __all_y_without_i(self, i, withLoss=False):
        """
        Get all y, from constraints as well as samples, but not the one for sample i.
        Return a list of (psi, number) tuples per y.
        """
        num_samples = self.__problem.num_samples
        all_y_not_i = []

        if withLoss:
            all_y_not_i += [(psi_i, l_i, nr_i) for c in range(num_samples) if c in self.__constraints_per_sample for psi_i, l_i, nr_i
                       in self.__constraints_per_sample[c]]
        else:
            all_y_not_i += [(psi_i, nr_i) for c in range(num_samples) if c in self.__constraints_per_sample for psi_i, l, nr_i
                       in self.__constraints_per_sample[c]]

        return all_y_not_i

    def _update_weights(self):
        w = np.zeros(self.__problem.num_dimensions)
        for s_i in range(self.__problem.num_samples):
            for psi_y, nr_y in self.__all_y_without_i(s_i):
                w += self.__dual_vars[(s_i, nr_y)].getAttr(gurobipy.GRB.attr.X) * (self.__psi_gt[s_i] - psi_y)
        self.__weights = w

    def find_largest_slack(self, constraints_for_i, psi_gt_i):
        all_xsis = [0.0]
        for psi_y, loss_y, nr_y in constraints_for_i:
            all_xsis.append(self.__compute_H_y(psi_gt_i, psi_y, loss_y))
        print("Slacks: {}".format(all_xsis))
        xsi = max(all_xsis)
        return xsi

    def solve(self, weights=None):
        """
        Solve the quadratic problem using gurobi, 
        instanciating constraints in a cutting plane approach as explained in
        "LARGE MARGIN METHODS FOR STRUCTURED AND INTERDEPENDENT OUTPUT VARIABLES"
        by Tsochantaridis, Algorithm 1

        Currently it only computes nonnegative weights!
        """
        if weights == None:
            weights = np.zeros(self.__problem.num_dimensions)

        self.__weights = weights
        new_constraints_added = True
        num_samples = self.__problem.num_samples
        constraint_nr = num_samples  # the first constraint id's are reserved to access the dual vars for all samples

        start_time = time.time()

        while new_constraints_added:
            new_constraints_added = False

            for i in range(num_samples):
                # get constraints related to sample i
                if not i in self.__constraints_per_sample:
                    self.__constraints_per_sample[i] = []
                constraints_for_i = self.__constraints_per_sample[i]

                # get ground truth psi for sample i
                psi_gt_i = self.__psi_gt[i]

                # find most violated constraint (H(y) in paper)
                loss, psi = self.__problem.separation_oracle(i, self.__weights)
                psi = np.array(psi)

                # determine value of current slack variable
                xsi = self.find_largest_slack(constraints_for_i, psi_gt_i)

                # check whether we add new constraints (=cutting planes)
                if self.__compute_H_y(psi_gt_i, psi, loss) > xsi + self.__epsilon:
                    print("Adding new constraint, because slack was exceeded ({} > {}+{})".format(
                        self.__compute_H_y(psi_gt_i, psi, loss), xsi, self.__epsilon))
                    new_constraints_added = True

                    # add constraint to working set
                    nr = constraint_nr
                    constraint_nr += 1
                    constraints_for_i.append( (psi, loss, nr) )
                    self.__constraints_per_sample[i] = constraints_for_i

                    # add new dual variable to the model
                    for s_i in range(num_samples):
                        self.__dual_vars[(s_i, nr)] = self.__model.addVar()
                    self.__model.update()

                    # add constraint to gurobi
                    print("New constraint is: {} <= {}".format(
                        sum([self.__dual_vars[(i, n)] / l for p, l, n in self.__all_y_without_i(i, True)]),
                        float(self.__problem.C) / num_samples))
                    self.__model.addConstr(sum([self.__dual_vars[(i, n)] / l for p, l, n in self.__all_y_without_i(i, True)])
                                               <= float(self.__problem.C) / num_samples)
                    self.__model.update()

                    # update objective to incorporate new dual vars
                    quad_obj = gurobipy.QuadExpr()

                    for s_i in range(num_samples):
                        # for each sample i, run over all y != y_i in the output space Y
                        for psi_i, nr_i in self.__all_y_without_i(s_i):
                            for s_j in range(num_samples):
                                for psi_j, nr_j in self.__all_y_without_i(s_j):
                                    J = np.dot(np.array(self.__psi_gt[s_i]) - np.array(psi_i),
                                               np.array(self.__psi_gt[s_j]) - np.array(psi_j))
                                    quad_obj += self.__dual_vars[(s_i, nr_i)] * self.__dual_vars[(s_j, nr_j)] * J

                    lin_obj = gurobipy.LinExpr()
                    for s_i in range(num_samples):
                        for psi_i, nr_i in self.__all_y_without_i(s_i):
                            lin_obj += self.__dual_vars[(s_i, nr_i)]
                    
                    self.__model.setObjective(-0.5 * quad_obj + lin_obj, gurobipy.GRB.MAXIMIZE)

                    # perform full QP optimization
                    self.__model.optimize()

                    # update weights
                    self._update_weights()
                else:
                    print("No new constraint was violated ({} <= {} + {}). Stopping.".format(
                        self.__compute_H_y(psi_gt_i, psi, loss), xsi, self.__epsilon))
        
        print("Number of cutting planes added: {}".format(constraint_nr))
        end_time = time.time()
        print("Done in {} secs".format(end_time - start_time))

        return self.__weights            


