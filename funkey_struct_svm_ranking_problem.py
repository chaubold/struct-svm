import argparse
import numpy as np
from funkey_problem import *
from struct_svm_solver_primal import *
import utils


class funkey_struct_svm_ranking_problem(funkey_problem):
    def __init__(self,
                 features_filename,
                 constraints_filename,
                 labels_filename,
                 proposals_filename):
        super(funkey_struct_svm_ranking_problem, self).__init__(features_filename, constraints_filename, labels_filename)

        self.num_proposals = 0
        self._proposals = np.zeros(1)

        self._load_proposals(proposals_filename)

    def get_best_proposal_for_sample(self, sample_idx):
        assert(sample_idx == 0)
        # TODO: how do we choose the best of the proposals? is it the one with lowest loss? lowest MAP energy?

        # going for lowest loss here.
        losses = np.array([utils.hamming_loss(self._ground_truth_label, p) for p in self._proposals])
        return losses.argmin()

    def get_proposal_feature_vector(self, sample_idx, proposal_idx):
        assert(sample_idx == 0)
        return self.make_psi(self._proposals[proposal_idx])

    def get_proposal_loss(self, sample_idx, proposal_idx):
        assert(sample_idx == 0)
        return utils.hamming_loss(self._ground_truth_label, self._proposals[proposal_idx])

    def get_num_proposals_for_sample(self, sample_idx):
        assert(sample_idx == 0)
        return self.num_proposals

    def _load_proposals(self, proposals_filename):
        """
        Loads the proposal labelings y0..yn from a file of the format:

        proposals.txt:

          0 1 # n labels per index variable, like labels.txt concatenated column wise
          1 1 # comments are allowed
          1 0
          1 1
        """
        with open(proposals_filename, 'rt') as proposals_file:
            self.num_proposals = -1
            proposal_labelings = []

            for line in proposals_file:
                line = utils.remove_comments_from_line(line)

                # ignore if line is a comment or empty
                if len(line) == 0 or line[0] == '#':
                    continue

                # read all labels and check that we always have the same number
                labels = line.split()

                if self.num_proposals < 0:
                    self.num_proposals = len(labels)
                else:
                    assert(self.num_proposals == len(labels))

                proposal_labelings.append(map(int, labels))

            self._proposals = np.array(proposal_labelings).transpose()
            assert(self._proposals.shape[1] == len(self._ground_truth_label))
            print("Loaded {} proposal labelings for {} variables".format(self.num_proposals, len(self._proposals)))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Struct-SVM ranking with augmented Funkey Learning interface.")
    parser.add_argument('--features', dest='features_filename', type=str, default="features.txt", help='Filename for features')
    parser.add_argument('--constraints', dest='constraints_filename', type=str, default="constraints.txt", help='Filename for constraints')
    parser.add_argument('--labels', dest='labels_filename', type=str, default="labels.txt", help='Filename for labels')
    parser.add_argument('--proposals', dest='proposals_filename', type=str, default="proposals.txt", help='Filename for proposals')
    parser.add_argument('--output', dest='weights_filename', type=str, default='weights.txt', help='Weights output filename')
    args = parser.parse_args()

    problem = funkey_struct_svm_ranking_problem(args.features_filename,
                                         args.constraints_filename,
                                         args.labels_filename,
                                         args.proposals_filename)
    solver = struct_svm_solver_primal(problem)
    weights = solver.solve()

    print("Found weights: {}".format(weights))
    outfile = file(args.weights_filename, 'wt')
    outfile.write("{}\n".format(weights))
    outfile.close()